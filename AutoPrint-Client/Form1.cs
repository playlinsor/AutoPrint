﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using AutoPrint_Client.Modules;

namespace AutoPrint_Client
{
    public partial class Form1 : Form
    {
        CodePrintStatus LastStatus = CodePrintStatus.UNKNOW;
        bool CloseForm = false;
        bool FirstHide = true;

        public Form1()
        {
            InitializeComponent();
            if (Properties.Settings.Default.AutoUpdate)
            {
                Properties.Settings.Default.Upgrade();
                Properties.Settings.Default.AutoUpdate = false;
            }

            try
            {
                textBox1.Text = Properties.Settings.Default.PathPrint;
                fileSystemWatcher1.Path = Properties.Settings.Default.PathPrint;
                UpdateInfo(textBox1.Text + "/status.txt");
            }
            catch (Exception ex)
            {
                label2.BackColor = Color.Salmon;
                MessageBox.Show("Ошибка в доступе к компьютеру\n" + ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            
        }

        /// <summary>
        /// Обновление информации о статусе печати
        /// </summary>
        /// <param name="FullPath">Путь к файлу статуса</param>
        private void UpdateInfo(string FullPath)
        {

            StatusInfo PrintStatus = new StatusInfo(FullPath);

            label1.Text = PrintStatus.FileName;
            if (LastStatus != PrintStatus.CurrentStatus)
            {
                Visible = true;
                Activate();
                LastStatus = PrintStatus.CurrentStatus;
            }

            Route(PrintStatus);

        }

        /// <summary>
        /// Роутер
        /// </summary>
        /// <param name="CS"></param>
        private void Route(StatusInfo CS)
        {
           
            label2.BackColor = Color.CornflowerBlue;
            switch (CS.CurrentStatus)
            {
                case CodePrintStatus.ErrorPrinting: Info_ErrorPrint(CS.FileName); break;
                case CodePrintStatus.ErrorInner: Info_ErrorPrintInner(CS.ErrorDesc);break;
                case CodePrintStatus.ErrorTimeOut: Info_ErrorTimeOut(CS.LastInfoData); break;
                case CodePrintStatus.ReadyOnPrint: Info_ReadyPrint(); break;
                case CodePrintStatus.PrinterOffline: Info_PrinterOffline(); break;
                case CodePrintStatus.WaitToPrint: Info_WaitPrint(); break;
                case CodePrintStatus.Printing: Info_Print(); break;
                case CodePrintStatus.EndPrint: Info_endPrint(); break;
                default: Info_UnknowPrint(); break;

            }
        }

        #region Функции роутера
        private void Info_ErrorPrint(string FileName)
        {
            Visible = true;
            label1.Visible = true;
            pictureBox1.Enabled = false;
            label2.Text = "ОШИБКА ПЕЧАТИ";
            label1.Text = FileName;
        }

        private void Info_ErrorPrintInner(string ErrorDesc)
        {
            Visible = true;
            label1.Visible = true;
            pictureBox1.Enabled = false;
            label2.Text = "Внутренняя ошибка печати";
            label1.Text = ErrorDesc;
        }

        private void Info_ErrorTimeOut(DateTime FileName)
        {
            Visible = true;
            label2.BackColor = Color.Coral;
            label1.Visible = true;
            pictureBox1.Enabled = false;
            label2.Text = "Связь потеряна";
            label1.Text = "Последняя связь: "+FileName.ToString();
        }

        private void Info_ReadyPrint()
        {
            pictureBox1.Enabled = false;
            label1.Visible = false;
            label2.Text = "Готов к печати";
            TopMost = false;
        }

        private void Info_PrinterOffline()
        {
            Visible = true;
            pictureBox1.Enabled = false;
            TopMost = true;
            label1.Visible = true;
            label2.Text = "Принтер выключен";
        }

        private void Info_WaitPrint()
        {
            Visible = true;
            TopMost = true;
            label1.Visible = true;
            label2.Text = "Подготовка к печати";
        }
        private void Info_Print()
        {
            Visible = true;
            TopMost = true;
            label2.Text = "Печатаю";
            pictureBox1.Enabled = true;
        }
        private void Info_endPrint()
        {
            Visible = true;
            label2.Text = "Готово";
            TopMost = false;
        }
        private void Info_UnknowPrint()
        {
            Visible = true;
            TopMost = false;
            label2.Text = "Общая ошибка";
            pictureBox1.Enabled = false;
            label1.Visible = true;
        }

        #endregion
        
        private void fileSystemWatcher1_Changed(object sender, FileSystemEventArgs e)
        {
            UpdateInfo(e.FullPath);
        }

        private void показатьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Visible) Visible = false;
            else Visible = true;
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseForm = true;
            Close();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Visible = false;
            if (!CloseForm)
                if (e.CloseReason.ToString() == "UserClosing") e.Cancel = true;

            if (FirstHide)
            {
                notifyIcon1.BalloonTipIcon = ToolTipIcon.Info;
                notifyIcon1.BalloonTipText = "Я свернулся сюда";
                notifyIcon1.BalloonTipTitle = "Информ панель AutoXPSPrint";
                notifyIcon1.ShowBalloonTip(1);
                FirstHide = false;
            }
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            /*
            notifyIcon1.BalloonTipIcon = ToolTipIcon.Info;
            notifyIcon1.BalloonTipText = string.Format("Статус - {0}",label2.Text);
            notifyIcon1.BalloonTipTitle = "Информ панель AutoXPSPrint";
            notifyIcon1.ShowBalloonTip(1);
             */

            if (Visible) Visible = false;
            else Visible = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                fileSystemWatcher1.Path = textBox1.Text;
                UpdateInfo(textBox1.Text + "/status.txt");
                label2.BackColor = Color.CornflowerBlue;
                Properties.Settings.Default.PathPrint = textBox1.Text;
                Properties.Settings.Default.Save();

            }
            catch (Exception ex)
            {
                label2.BackColor = Color.Salmon;
                MessageBox.Show("Ошибка в доступе к компьютеру\n" + ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void выходToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AutoPrint_Client ab = new AutoPrint_Client();
            ab.ShowDialog();
        }


    }
}
